require('./bootstrap');
import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter)

import main from './views/page/main'
import database from './views/page/constructor/database'
import database_edit from './views/page/constructor/database_edit'
import database_bread from './views/page/constructor/database_bread'
import maps from './views/page/database/maps'

import _browse from './views/page/database/browse'
import _edit from './views/page/database/edit'

//Auth
import login from './views/auth/login'

import {get_user} from "../../overall/js/app";

// console.log(window.laravel.api_token);
var user_auth = get_user();

var routers = [
    {path: '/*', component: login},
];
//
if (user_auth.success) {
    routers = [
        {path: '/admin', component: main},
        {path: '/admin/database', component: database},
        {path: '/admin/database/:model/edit', component: database_edit},
        {path: '/admin/database/:model/bread', component: database_bread},
        {path: '/admin/:model/browse', component: _browse},
        {path: '/admin/:model/edit/:id', component: _edit},
        {path: '/admin/maps', component: maps}
    ];
}

export default new VueRouter({
    mode: 'history',
    routes: routers
});
