<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//Route::post('/ferialfile', 'Api\Admin\MainController@ferialfile');
//Route::post('/checkgile', 'Api\Admin\MainController@checkgile');

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/crest/install', 'MainController@installApp');
Route::post('/crest/install', 'MainController@installAppPost');

Route::post('/admin/delete_point', 'Api\Admin\MainController@delete_point');
Route::post('/admin/add_point', 'Api\Admin\MainController@add_point');
Route::post('/admin/update_point', 'Api\Admin\MainController@update_point');

Route::get('/admin/get_point/{id}', 'Api\Admin\MainController@get_point');

Route::get('/auth/user', 'Api\AuthController@get_user');
Route::post('/auth/login', 'Api\AuthController@auth_login');

Route::get('/admin/type_row_db', 'Api\Admin\MainController@typeRowDb');

Route::group(['prefix' => '/table/'], function () {
    Route::post('/update', 'Api\Admin\Constructor\TableController@update');
    Route::post('/delete', 'Api\Admin\Constructor\TableController@delete');
    Route::get('/list', 'Api\Admin\Constructor\TableController@table_list');
    Route::get('/list/{model}', 'Api\Admin\Constructor\TableController@table_list_model');
});

Route::group(['prefix' => '/database'], function () {
    Route::group(['prefix' => '/bread/{model}'], function () {
        Route::get('/get', 'Api\Admin\Constructor\BreadController@database_bread_get');
        Route::post('/update', 'Api\Admin\Constructor\BreadController@database_bread_update');
    });
    Route::group(['prefix' => '/{model}'], function () {
        Route::get('/browse_column', 'Api\Admin\Database\MainController@browse_column_get');
        Route::post('/delete', 'Api\Admin\Database\MainController@browse_delete');
        Route::get('/edit_column/{id}', 'Api\Admin\Database\MainController@edit_column_get');
    });
    Route::post('/update', 'Api\Admin\Database\MainController@database_update');
    Route::get('/model_list', 'Api\Admin\Database\MainController@database_list');
    Route::get('/get/{model}/{id}', 'Api\Admin\Database\MainController@database_single_get');
    Route::get('/get/{model}', 'Api\Admin\Database\MainController@database_all_get');
});
