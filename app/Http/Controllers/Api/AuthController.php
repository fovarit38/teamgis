<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AuthController extends BaseController
{


    protected $users;


    public function get_user()
    {
        $this->users = auth()->guard('api')->user();
        if (empty($this->users)) {

            return $this->sendError('вы не авторизованы');
        } else {
            return $this->sendResponse('вы авторизованы', $this->users);
        }
    }


    public function auth_login(Request $request)
    {

        $nameInput = "login";
        $fieldName = "";
        $identity = $request->login;
        if (filter_var($identity, FILTER_VALIDATE_EMAIL)) {
            $fieldName = "email";
        } else {
            $fieldName = validate_phone_number($identity) == false ? 'username' : 'tel';
        }
        if ($fieldName == "tel") {
            $identity = validate_phone_number($identity);
        }

        $success = [];

        if (Auth::attempt([$fieldName => $identity, 'password' => $request->password])) {
            $user = Auth::user();
            $success['token'] = $user->createToken(env("APP_NAME"))->accessToken;
            return $this->sendResponse($success, 'Вы авторизированны успешно.');
        } else {
            return $this->sendError('Error', ['error' => 'Введен неверный логин или пароль']);
        }
    }


    public function register(Request $request)
    {

        $fieldName = "tel";
        $identity = validate_phone_number($request->tel);

//        if($identity==false){
//            return $this->sendError('Ошибка валидации.', $validator->errors());
//        }

        request()->merge([$fieldName => $identity]);

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'password' => 'required',
            'password_confirmation' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Ошибка валидации.', $validator->errors());
        }


        $input = $request->all();


        if (isset($input["email"])) {
            $email = \App\User::where("email", $input["email"])->first();
            if (!is_null($email)) {
                return $this->sendError('Error', ['error' => 'email уже занят']);
            }
        }

        if (isset($input["tel"])) {
            $phone = \App\User::where("tel", $input["tel"])->first();
            if (!is_null($phone)) {
                return $this->sendError('Error', ['error' => 'Телефон уже занят']);
            }
        }

        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] = $user->createToken('burgerApp')->accessToken;
        $success['name'] = $user->name;
        $success['email'] = $user->email;
        $success['tel'] = $user->tel;
        $success['address'] = $user->address;
        $success['apartment'] = $user->apartment;
        $success['entrance'] = $user->entrance;
        $success['district'] = $user->district;


        return $this->sendResponse($success, 'Пользователь создан успешно.');
    }


}
