<?php

namespace App\Http\Controllers\Api\Admin\Constructor;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use TableClass;

class BreadController extends BaseController
{
    protected $users;


    public function __construct(Request $request)
    {
//        $this->users = auth()->guard('api')->user();
//
//        if (is_null($this->users) || !isset($this->users->id) || ($this->users) == "") {
//            $this->middleware(function ($request, $next) {
//                return $this->sendError('вы не авторизованы');
//            });
//        }
    }

    public function database_bread_get($model)
    {
        $model = app("\App\\Models\\" . $model);
        $table_name = $model->getTable();
        $path = "../database/json/bread/" . $table_name . ".json";

        if (file_exists($path)) {
            $model_list = file_get_contents($path);
            return $this->sendResponse(json_decode($model_list,true), '');
        }
        return $this->sendResponse([], '');
    }

    public function database_bread_update($model, Request $request)
    {
        $request = $request->all();
        $table = new TableClass($request["table"]);
        $table->bread($request["field"]);

//        return $this->sendResponse($request["row"], '');
    }




}
