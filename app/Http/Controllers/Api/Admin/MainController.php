<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use NCANodeClient;

class MainController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //Товары в корзине
    protected $users;


    public function __construct(Request $request)
    {
//        $this->users = auth()->guard('api')->user();
//
//        if (is_null($this->users) || !isset($this->users->id) || ($this->users) == "") {
//            $this->middleware(function ($request, $next) {
//                return $this->sendError('вы не авторизованы');
//            });
//        }
    }

    public function ferialfile(Request $request)
    {
        $request = $request->all();

        $file_type = $request["document"]->getClientOriginalExtension();


        $nca = new NCANodeClient('http://127.0.0.1:14579');
        $p12InBase64 = base64_encode((file_get_contents($request["ecp"]->getRealPath())));
        $document = base64_encode(file_get_contents(($request["document"]->getRealPath())));
        $document_cms = $nca->rawSign($document, $p12InBase64, $request["password"]);
        $text = base64_decode($document_cms["result"]["cms"]);

        $file_name = Str::random(4) . "." . $file_type . ".cms";
        $fp = fopen($file_name, "w");
        fwrite($fp, $text);
        fclose($fp);
        return $this->sendResponse($file_name, '');

    }

    public function checkgile(Request $request)
    {
        $request = $request->all();
        $nca = new NCANodeClient('http://127.0.0.1:14579');
        $document = base64_encode(file_get_contents(($request["document"]->getRealPath())));
        $document_cms = $nca->cmsVerify($document);
        return $this->sendResponse($document_cms["result"], '');
    }

    public function typeRowDb()
    {
        $resoebs = get_type_sql();
        foreach ($resoebs as $index => $ksa) {
            $resoebs[$index] = array_values($ksa);
        }
        return $this->sendResponse($resoebs, '');
    }

    public function delete_point(Request $request)
    {
        $request = $request->all();
        \App\Models\RestaurantScheme::find($request["id"])->delete();
    }

    public function get_point($id)
    {
        $return_items = [];
        $restauranHall = \App\Models\RestaurantScheme::where("restaurant_hall_id", $id)->get();
        foreach ($restauranHall as $item_res) {
            $scheme_element = \App\Models\SchemeElements::find($item_res->scheme_elements_id)->toarray();
            if (!is_null($scheme_element)) {
                $scheme_element["offsety"] = $item_res->offsety;
                $scheme_element["offsetx"] = $item_res->offsetx;
                $scheme_element["name"] = $item_res->name;
                $scheme_element["rotation"] = $item_res->rotation;
                $scheme_element["id"] = $item_res->id;
                array_push($return_items, $scheme_element);
            }
        }
        return $this->sendResponse($return_items, '');
    }

    public function update_point(Request $request)
    {
        $request = $request->all();
        $restauranHall = \App\Models\RestaurantScheme::find($request["id"]);
        if (!is_null($restauranHall)) {
            $restauranHall->offsety = $request["offsety"];
            $restauranHall->offsetx = $request["offsetx"];
            $restauranHall->save();
        }

    }

    public function add_point(Request $request)
    {
        $request = $request->all();
        $restauranHall = new \App\Models\RestaurantScheme;
        $restauranHall->restaurant_hall_id = $request["restaurant_hall_id"];
        $restauranHall->scheme_elements_id = $request["scheme_elements_id"];
        $restauranHall->offsety = $request["offsety"];
        $restauranHall->offsetx = $request["offsetx"];
        $restauranHall->name = $request["name"];
        $restauranHall->rotation = 0;
        $restauranHall->save();

    }

}
