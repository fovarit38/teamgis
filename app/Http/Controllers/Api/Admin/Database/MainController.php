<?php

namespace App\Http\Controllers\Api\Admin\Database;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use TableClass;
use Storage;
use Str;
use NCANodeClient;
use Illuminate\Support\Facades\Hash;
use Schema;

class MainController extends BaseController
{
    protected $users;


    public function __construct(Request $request)
    {
//        $this->users = auth()->guard('api')->user();
//
//        if (is_null($this->users) || !isset($this->users->id) || ($this->users) == "") {
//            $this->middleware(function ($request, $next) {
//                return $this->sendError('вы не авторизованы');
//            });
//        }
    }


    public function edit_column_get($modul, $id)
    {

        if (!file_exists("../app/Models/" . $modul . ".php")) {
            return $this->sendResponse([], 'error');
        }

        $model = app("\App\\Models\\" . $modul);


        $table = $model->getTable();
        $crow = "add";
        if ($id != "0") {
            $model = $model::find($id);
            if (!is_null($model)) {
                $crow = "edit";
            }
        }


        if (!file_exists("../database/json/bread/" . $table . ".json")) {
            return $this->sendResponse([], 'error');
        }
        if (!file_exists("../database/json/" . $table . ".json")) {
            return $this->sendResponse([], 'error');
        }

        $model_list = file_get_contents("../database/json/bread/" . $table . ".json");
        $model_list = json_decode($model_list, true);

        $request = file_get_contents("../database/json/" . $table . ".json");
        $request = json_decode($request, true);

        $thead_return = [];
        foreach ($model_list as $name => $thead) {
            if (isset($thead["views"])) {
                if (in_array($crow, $thead["views"])) {
                    $model_has_one = [];
                    if ($thead["type"] == "Image") {
                        $thead["type"] = "file";
                    } else if ($thead["type"] == "hasOne") {
                        $model_has_one = app("\App\\Models\\" . $thead["hasone"]);
                        $model_has_one = $model_has_one->get()->toarray();
                        if ($thead["hasone"] == "RestaurantScheme") {
                            foreach ($model_has_one as $index => $lms) {

                                $hall = \App\Models\RestaurantHall::find($lms["restaurant_hall_id"]);
                                if (!is_null($hall)) {
                                    $restoran = \App\Models\Restaurant::first();
//                                    \App\Models\Restaurant::
                                    $model_has_one[$index]["name"] = $restoran->name . " / " . $hall->name . " / " . $lms["name"];
                                }
                            }
                        }
                    }
                    $input_info = array_filter($request, function ($value) use ($name) {
                        return $value['name'] == $name;
                    });
                    sort($input_info);
                    $input_info = $input_info[0];

                    if (isset($thead["display"]) ? (str_replace(" ", "", $thead["display"]) != "" ?: false) : false) {

                        array_push($thead_return, ["name" => $name, "placeholder" => $thead["display"], "type" => $thead["type"], "hasOne" => $model_has_one, "required" => (isset($input_info["notnull"]) ? true : false)]);

                    } else {
                        array_push($thead_return, ["name" => $name, "placeholder" => $name, "type" => $thead["type"], "hasOne" => $model_has_one, "required" => (isset($input_info["notnull"]) ? true : false)]);
                    }
                }
            }
        }
        return $this->sendResponse($thead_return, '');
    }

    public function browse_column_get($modul)
    {
        if (!file_exists("../app/Models/" . $modul . ".php")) {
            return $this->sendResponse([], 'error');
        }

        $model = app("\App\\Models\\" . $modul);
        $table = $model->getTable();

        if (!file_exists("../database/json/bread/" . $table . ".json")) {
            return $this->sendResponse([], 'error');
        }

        $model_list = file_get_contents("../database/json/bread/" . $table . ".json");
        $model_list = json_decode($model_list, true);


        $thead_return = [];
        foreach ($model_list as $name => $thead) {
            if (isset($thead["views"])) {
                if (in_array("browse", $thead["views"])) {
                    if (isset($thead["display"]) ? (str_replace(" ", "", $thead["display"]) != "" ?: false) : false) {
                        $thead_return[$name] = $thead["display"];
                    } else {
                        $thead_return[$name] = $name;

                    }
                }
            }
        }
        return $this->sendResponse($thead_return, '');
    }

    public function database_single_get($model, $id)
    {

        if (!file_exists("../app/Models/" . $model . ".php")) {
            return $this->sendResponse([], 'error');
        }

        $model_name = $model;
        $model = app("\App\Models\\$model_name");
        $model = $model->find($id);

        if (is_null($model)) {
            return $this->sendResponse([], 'error');
        }

        return $this->sendResponse($model, '');
    }

    public function browse_delete($model, Request $request)
    {

        if (!file_exists("../app/Models/" . $model . ".php")) {
            return $this->sendResponse([], 'error');
        }

        $model_name = $model;
        $model = app("\App\Models\\$model_name");
        $model = $model->find($request["id"]);
        if (is_null($model)) {
            return $this->sendResponse([], 'error');
        }
        if ($model_name == "RestaurantHall") {
            $re_shema = \App\Models\SchemeElements::where("restaurant_id", $model->id)->get();
            foreach ($re_shema as $rs_s) {
                $rs_s->delete();
            }
        }
        $model->delete();
        return $this->sendResponse($model, '');
    }

    public function database_all_get($model, Request $request)
    {
        $request = $request->all();
        if (!file_exists("../app/Models/" . $model . ".php")) {
            return $this->sendResponse([], 'error');
        }


        $model_name = $model;
        $model = app("\App\Models\\$model_name");
        if (isset($request["restaurant_id"])) {
            $model = $model->where("restaurant_id", $request["restaurant_id"]);
        }
        if (isset($request["search"])) {
            $model_na = app("\App\Models\\$model_name");
            $columns = Schema::getColumnListing($model_na->getTable());
            $model = $model->where(function ($query) use ($columns, $request) {
                foreach ($columns as $coco) {
                    $query->orWhere($coco, "LIKE", "%" . $request["search"] . "%");
                }

            });
        }
        $model = $model->get();

        if (is_null($model)) {
            return $this->sendResponse([], 'error');
        }
        return $this->sendResponse($model, '');
    }

    public function database_list()
    {
        $model_list = json_decode(file_get_contents("../database/db_list.json"), true);
        $models = [];
        foreach ($model_list as $model) {
            array_push($models, $model["name"]);
        }
        return $this->sendResponse($models, '');
    }

    public function database_update(Request $request)
    {
        $files = $request;
        $request = $request->all();
        if (!file_exists("../app/Models/" . $request["model_name"] . ".php")) {
            return $this->sendResponse([], 'error');
        }


        if (isset($request["brief"])) {
            foreach ($request["brief"] as $index => $brifs) {
                $briafs = \App\Models\Brief::find($index);
                if (!is_null($briafs)) {
                    $briafs->icon = $brifs["icon"];
                    $briafs->content = $brifs["content"];
                    $briafs->save();
                }
            }
        }
        $model_list = array_filter(json_decode(file_get_contents("../database/db_list.json"), true), function ($value) use ($request) {
            return $value['model'] == $request["model_name"];
        });
        sort($model_list);

        $save_date = [];
        $model_row = [];
        if (isset($model_list[0])) {
            $model_list = $model_list[0];
            if (file_exists("../database/json/bread/" . $model_list["table"] . ".json")) {
                $model_row = file_get_contents("../database/json/bread/" . $model_list["table"] . ".json");
                $model_row = json_decode($model_row, true);
            }
        }

        $id = $request["id"];
        $model_name = $request["model_name"];
        $model = null;
        if ($id == 0) {
            $model = app("\App\Models\\$model_name");
        } else {
            $model = app("\App\Models\\$model_name");
            $model = $model->find($id);
        }

        if (method_exists($model, 'preprocessing')) {
            $request = $model->preprocessing($request);
        }

        if (isset($_FILES["save"])) {
            foreach ($_FILES["save"]["name"] as $nameInput => $fileout) {
                if ($files->hasFile("save." . $nameInput)) {
                    $filename = Str::random(6) . "." . $files["save"][$nameInput]->getClientOriginalExtension();
                    $files_is = Storage::disk('uploads')->put($filename, file_get_contents($files["save"][$nameInput]->getRealPath()));
                    if ($files_is) {
                        $request["save"][$nameInput] = "/storage/uploads/" . $filename;
                    } else {
                        unset($request["save"][$nameInput]);
                    }
                } else {
                    unset($request["save"][$nameInput]);
                }
            }
        }

        foreach ($request["save"] as $key => $input) {
            if (is_array($input)) {
                $model->{$key} = json_encode($input, JSON_UNESCAPED_UNICODE);
            } else {

                $type_save = isset($model_row[$key]) ? $model_row[$key] : "string";
                if ($type_save["type"] == "Password") {
                    if (!empty($input)) {
                        $model->{$key} = Hash::make($input);
                    }
                } else {
                    $model->{$key} = $input;
                }
            }
        }


        $model->save();

        if ($request["model_name"] == "Restaurant" && $id == "0") {
            $ar_box = [
                ['name' => 'Прямоугольный стол 4 кресла', 'group' => 'def 4', 'width' => '245', 'height' => '192', 'icon' => '/media/client/images/icon/p4.svg'],
                ['name' => 'Прямоугольный стол 6 кресел', 'group' => 'def 6', 'width' => '245', 'height' => '322', 'icon' => '/media/client/images/icon/p6.svg'],
                ['name' => 'Прямоугольный стол 8 кресел', 'group' => 'def 8', 'width' => '245', 'height' => '407', 'icon' => '/media/client/images/icon/p8.svg'],
                ['name' => 'Прямоугольный стол 10 кресел', 'group' => 'def 10', 'width' => '245', 'height' => '496', 'icon' => '/media/client/images/icon/p10.svg'],
                ['name' => 'Прямоугольный стол 2 Дивана', 'group' => 'def 2', 'width' => '253', 'height' => '192', 'icon' => '/media/client/images/icon/dv2.svg'],
                ['name' => 'Прямоугольный стол 2 Дивана мини', 'group' => 'def 2', 'width' => '252', 'height' => '192', 'icon' => '/media/client/images/icon/dvk4mini.svg'],
                ['name' => 'Круглый стол 6 кресел', 'group' => 'def 6', 'width' => '306', 'height' => '312', 'icon' => '/media/client/images/icon/k6.svg'],
                ['name' => 'Круглый стол 8 кресел', 'group' => 'def 8', 'width' => '409', 'height' => '408', 'icon' => '/media/client/images/icon/k8.svg'],
                ['name' => 'Круглый стол 10 кресел', 'group' => 'def 10', 'width' => '410', 'height' => '408', 'icon' => '/media/client/images/icon/k10.svg'],
                ['name' => 'Круглый стол 2 кресла', 'group' => 'def 2', 'width' => '102', 'height' => '227', 'icon' => '/media/client/images/icon/kv2.svg'],
                ['name' => 'Квадратный стол 4 кресла', 'group' => 'def 4', 'width' => '237', 'height' => '236', 'icon' => '/media/client/images/icon/kv4.svg'],
                ['name' => 'Квадратный стол 4 Дивана ', 'group' => 'def 4', 'width' => '330', 'height' => '330', 'icon' => '/media/client/images/icon/dvk4.svg'],
                ['name' => 'Квадратный стол 2 Дивана 2 кресла', 'group' => 'def 4', 'width' => '329', 'height' => '252', 'icon' => '/media/client/images/icon/dvk2s2.svg'],
            ];
            foreach ($ar_box as $s_save) {
                $schemeelements = new \App\Models\SchemeElements;
                $schemeelements->name = $s_save["name"];
                $schemeelements->restaurant_id = $model->id;
                $schemeelements->width = $s_save["width"];
                $schemeelements->height = $s_save["height"];
                $schemeelements->icon = $s_save["icon"];
                $schemeelements->group = $s_save["group"];
                $schemeelements->save();
            }
        }

        return $this->sendResponse([$model], '');

    }


}
