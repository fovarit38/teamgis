<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    public function __construct($app)
    {
        $model_list = file_get_contents(base_path()."/database/db_list.json");
        $molde_list = json_decode($model_list, true);
        foreach ($molde_list as $model) {
            if (file_exists(base_path().'/app/Policies/' . $model["model"] . 'Policy.php')) {
                $this->policies['App\Models\\' . $model["model"]] = 'App\Policies\\' . $model["model"] . 'Policy';
            }
        }
        parent::__construct($app);
    }

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();
    }
}
